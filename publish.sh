#!/bin/bash

cd publish/

CURSEFORGE_TOKEN=$(cat ~/auth_tokens/Curseforge) MODRINTH_TOKEN=$(cat ~/auth_tokens/Modrinth) pipenv run python publish.py "$@"
