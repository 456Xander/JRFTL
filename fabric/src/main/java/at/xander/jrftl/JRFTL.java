package at.xander.jrftl;

import net.fabricmc.api.ModInitializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JRFTL implements ModInitializer {
	public static final String MODID = "jrftl";

    public static final Logger LOGGER = LoggerFactory.getLogger(MODID);

	@Override
	public void onInitialize() {
		JrftlItems.initialize();
	}
}