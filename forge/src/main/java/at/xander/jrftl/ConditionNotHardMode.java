package at.xander.jrftl;

import com.mojang.serialization.DynamicOps;
import com.mojang.serialization.MapCodec;

import net.minecraftforge.common.crafting.conditions.ICondition;

/**
 * I cannot get forge:not condition to work, so implement our own inverted condition
 */
public class ConditionNotHardMode implements ICondition {
    public static final MapCodec<ConditionNotHardMode> CODEC = MapCodec.unit(ConditionNotHardMode::new);

	@Override
	public MapCodec<? extends ICondition> codec() {
		return CODEC;
	}
	
	@Override
	public String toString() {
		return "jrftl_is_not_hardmode()";
	}

	@Override
	public boolean test(IContext context, DynamicOps<?> ops) {
		return !JRFTL.instance.isHardMode();
	}

}
