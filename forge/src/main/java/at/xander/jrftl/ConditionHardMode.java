package at.xander.jrftl;

import com.mojang.serialization.DynamicOps;
import com.mojang.serialization.MapCodec;

import net.minecraftforge.common.crafting.conditions.ICondition;

public class ConditionHardMode implements ICondition {
    public static final MapCodec<ConditionHardMode> CODEC = MapCodec.unit(ConditionHardMode::new);

	@Override
	public MapCodec<? extends ICondition> codec() {
		return CODEC;
	}
	
	@Override
	public String toString() {
		return "jrftl_is_hardmode()";
	}

	@Override
	public boolean test(IContext context, DynamicOps<?> ops) {
		return JRFTL.instance.isHardMode();
	}

}
