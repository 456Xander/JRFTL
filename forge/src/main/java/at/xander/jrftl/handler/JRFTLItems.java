package at.xander.jrftl.handler;

import com.mojang.serialization.MapCodec;

import at.xander.jrftl.ConditionHardMode;
import at.xander.jrftl.ConditionNotHardMode;
import at.xander.jrftl.JRFTL;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraftforge.client.event.ModelEvent;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Function;

public class JRFTLItems {
    // Registries
    private final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, JRFTL.MODID);
    private final DeferredRegister<MapCodec<? extends ICondition>> CONDITION_SERIALIZERS = DeferredRegister
            .create(ForgeRegistries.CONDITION_SERIALIZERS, JRFTL.MODID);

    // Helper Functions
    private ResourceKey<Item> jrftlItemId(String name) {
        return ResourceKey.create(Registries.ITEM, ResourceLocation.fromNamespaceAndPath(JRFTL.MODID, name));
    }

    private RegistryObject<Item> registerItem(String name, Function<Item.Properties, Item> constructor) {
        return registerItem(name, constructor, new Item.Properties());
    }

    private RegistryObject<Item> registerItem(String name, Function<Item.Properties, Item> constructor, Item.Properties props) {
        var propWithId = props.setId(jrftlItemId(name));
        return ITEMS.register(name, () -> constructor.apply(propWithId));
    }


    // Items
    public final RegistryObject<Item> PreparedFlesh = registerItem("prepared_flesh", Item::new);

    // Conditions
    public final RegistryObject<MapCodec<ConditionHardMode>> HardMode = CONDITION_SERIALIZERS.register("hard_mode",
            () -> ConditionHardMode.CODEC);
    public final RegistryObject<MapCodec<ConditionNotHardMode>> NotHardMode = CONDITION_SERIALIZERS.register("not_hard_mode",
            () -> ConditionNotHardMode.CODEC);

    // Init Function

    public JRFTLItems(FMLJavaModLoadingContext ctx) {
        ITEMS.register(ctx.getModEventBus());
        CONDITION_SERIALIZERS.register(ctx.getModEventBus());
        ctx.getModEventBus().addListener(this::handleCreativeModeTabPopulation);
    }

    private void handleCreativeModeTabPopulation(BuildCreativeModeTabContentsEvent event) {
        if (event.getTabKey() == CreativeModeTabs.INGREDIENTS) {
            if (JRFTL.instance.isHardMode()) {
                event.accept(PreparedFlesh.get());
            }
        }
    }
}
