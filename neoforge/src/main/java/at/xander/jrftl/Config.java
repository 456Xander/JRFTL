package at.xander.jrftl;

import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.event.config.ModConfigEvent;
import net.neoforged.neoforge.common.ModConfigSpec;


@EventBusSubscriber(modid = JRFTL.MODID, bus = EventBusSubscriber.Bus.MOD)
public class Config {
	private final ModConfigSpec.ConfigValue<Boolean> hardModeConfig;
	public final ModConfigSpec conf;

	public boolean isLoaded() {
		return conf.isLoaded();
	}

	public boolean isHardMode() {
		return hardModeConfig.get();
	}

	Config() {
		ModConfigSpec.Builder builder = new ModConfigSpec.Builder();
		builder.push("general");
		hardModeConfig = builder.comment(
				"In HardMode rotten flesh has first to be crafted into prepared flesh by crafting it in a 2x2 grid.",
				"The game has to be restarted for this config to take effect").define("enableHardMode", false);
		builder.pop();
		conf = builder.build();
	}

	@SubscribeEvent
	public static void onLoad(final ModConfigEvent configEvent) {
		JRFTL.instance.logger.debug("Loaded JRFTL config file {}", configEvent.getConfig().getFileName());
		JRFTL.instance.logger.debug("hard_mode={}", JRFTL.instance.isHardMode());


	}

}
