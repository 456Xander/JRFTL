package at.xander.jrftl.handler;

import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;

import at.xander.jrftl.ConditionHardMode;
import at.xander.jrftl.JRFTL;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.ModelEvent;
import net.neoforged.neoforge.common.conditions.ICondition;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

import java.util.function.Function;


public class JRFTLItems {
	//Registries
	private final DeferredRegister<Item> ITEMS = DeferredRegister.create(BuiltInRegistries.ITEM, JRFTL.MODID);
	private final DeferredRegister<MapCodec<? extends ICondition>> CONDITION_SERIALIZERS = DeferredRegister.create(NeoForgeRegistries.CONDITION_SERIALIZERS, JRFTL.MODID);

	// Helper Functions
	private ResourceKey<Item> jrftlItemId(String name) {
		return ResourceKey.create(Registries.ITEM, ResourceLocation.fromNamespaceAndPath(JRFTL.MODID, name));
	}

	private DeferredHolder<Item, Item> registerItem(String name, Function<Item.Properties, Item> constructor) {
		return registerItem(name, constructor, new Item.Properties());
	}

	private DeferredHolder<Item, Item> registerItem(String name, Function<Item.Properties, Item> constructor, Item.Properties props) {
		var propWithId = props.setId(jrftlItemId(name));
		return ITEMS.register(name, () -> constructor.apply(propWithId));
	}

	// Items
	public final DeferredHolder<Item, Item> PreparedFlesh = registerItem("prepared_flesh", Item::new);
	
	// Conditions
	public final DeferredHolder<MapCodec<? extends ICondition>, MapCodec<ConditionHardMode>> HardMode = CONDITION_SERIALIZERS.register("hard_mode", () -> ConditionHardMode.CODEC);
	
	// Init Function

	public JRFTLItems(IEventBus bus) {
		ITEMS.register(bus);
		CONDITION_SERIALIZERS.register(bus);
		bus.addListener(this::handleCreativeModeTabPopulation);
	}
	

	
	private void handleCreativeModeTabPopulation(BuildCreativeModeTabContentsEvent event) {
		if(event.getTabKey() == CreativeModeTabs.INGREDIENTS) {
			if (JRFTL.instance.isHardMode()) {
				event.accept(PreparedFlesh.get());
			}
		}
	}
}
