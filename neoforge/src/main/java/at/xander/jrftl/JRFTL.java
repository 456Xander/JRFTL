package at.xander.jrftl;

import org.slf4j.Logger;

import com.mojang.logging.LogUtils;

import at.xander.jrftl.handler.JRFTLItems;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.ModLoadingContext;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;

@Mod(JRFTL.MODID)
public class JRFTL {
	public static final String MODID = "jrftl";

	public static JRFTL instance;

	private final Config config = new Config();

	public final JRFTLItems items;

	public final Logger logger = LogUtils.getLogger();
	
	public JRFTL(ModContainer container) {
		container.getEventBus().addListener(this::clientInit);
		instance = this;
		container.registerConfig(ModConfig.Type.COMMON, config.conf);
		// Register Item Registries at the end
		items = new JRFTLItems(container.getEventBus());
	}

	private void clientInit(FMLClientSetupEvent e) {

	}

	public boolean isHardMode() {
		if (!config.isLoaded()) {
			System.out.println("Warning config not loaded");
		}
		return config.isHardMode();
	}

}
