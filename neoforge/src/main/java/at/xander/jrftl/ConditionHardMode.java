package at.xander.jrftl;

import com.mojang.serialization.MapCodec;

import net.neoforged.neoforge.common.conditions.ICondition;

public class ConditionHardMode implements ICondition {
    public static final MapCodec<ConditionHardMode> CODEC = MapCodec.unit(ConditionHardMode::new);

	@Override
	public boolean test(IContext context) {
		return JRFTL.instance.isHardMode();
	}

	@Override
	public MapCodec<? extends ICondition> codec() {
		return CODEC;
	}
	
	@Override
	public String toString() {
		return "jrftl_is_hardmode()";
	}

}
