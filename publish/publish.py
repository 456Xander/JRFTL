#!/bin/env python

from tap import Tap
from publishers import Target, get_publisher
from building import BuiltMod, Loader, build_mod
import logging
import coloredlogs

logger = logging.getLogger("ModPublisher")


class Args(Tap):
    prj: list[Loader] = [Loader.Forge, Loader.NeoForge, Loader.Fabric]
    """List of the subprojects to build and publish"""

    dry_run: bool = False
    """If true, do not actually push to curseforge and modrinth"""

    target: list[Target] = [Target.CurseForge, Target.Modrinth]
    """Where to publish to"""


if __name__ == "__main__":
    coloredlogs.install(level=logging.DEBUG)
    args = Args().parse_args()
    if args.dry_run:
        logger.info("Running in dry-run mode. Nothing will actually be published")
    mods = [build_mod(loader) for loader in args.prj]

    for target in args.target:
        publisher = get_publisher(target)
        for mod in mods:
            logging.info(f"Exporting {mod.loader} Mod to {target}")
            publisher.publish(mod, args.dry_run)
